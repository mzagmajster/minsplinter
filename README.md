# Minsplinter

Minimalistic and simple package useful for extensive web scraping and test automation.

## Getting Started

Follow instructions below to prepare project for development.

### Prerequisites

This project depends on Python 2.7.x, so you need to install it before you can use this project. Please note that
we are assuming that you are trying to install project into virtualenv with the help of
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/).

We have to place at least one web driver binary into PATH environment variable, to be able to use this project. Which
driver we use is up to each user. By default package uses firefox web driver (and so expects gecko driver).

* [Gecko driver](https://github.com/mozilla/geckodriver/releases)
* [Chrome driver](https://sites.google.com/a/chromium.org/chromedriver/home)

### Installing

Clone repository and move to project root.

Create virtual environment.

```
mkvirtualenv minsplinter
```

Install production and development dependencies.

```
python setup.py install -e .[dev]
```

## Running the tests

To run project's tests you execute command below.

```
python manage.py test
```

## Deployment

Use project in production environment.

```
pip setup.py install
```

## Changelog

We do not have ```CHANGELOG.md``` file yet, because package does not have official release yet.

## Documentation

Please refer to correct sub folder in repository [here](https://gitlab.com/Zag/public-docs) for details.

## Contributing

Please read ```CONTRIBUTING.md``` for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

Initial content for this project was provided by Matic Zagmajster. For more information please see [AUTHORS]() file.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
