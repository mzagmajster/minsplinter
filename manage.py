import click, unittest

from tests import TestMinsplinter


@click.group()
def cli():
	pass


@click.command('test')
def run_tests():
	suite = unittest.TestLoader().loadTestsFromTestCase(TestMinsplinter)
	unittest.TextTestRunner(verbosity=2).run(suite)


cli.add_command(run_tests)

if __name__ == '__main__':
	cli()
