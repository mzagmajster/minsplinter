from time import sleep
from splinter import Browser


class MinSplinterError(Exception):
	"""Basic package exception"""
	pass


class MinSplinter(object):
	"""Mainsplinter class"""

	def _is_valid_config(self):
		"""Validates object configuration.

		Returns:
			bool: True on successful validation, False otherwise.

		"""
		conf = ['DRIVER_NAME', 'USER_AGENT', 'PROFILE']

		for key in self.config.iterkeys():
			if key not in conf:
				raise MinSplinterError('Config validation failed for key {0}.'.format(key))

		return True

	def __init__(self, config):
		"""Initializer"""
		self.config = {
			'DRIVER_NAME': 'firefox',
			'USER_AGENT': None,
		}
		self.config.update(config)
		self._is_valid_config()

		# General config.
		bconf = dict()
		bconf['driver_name'] = self.config['DRIVER_NAME']
		bconf['user_agent'] = self.config['USER_AGENT']
		
		# Driver specific.
		if self.config['DRIVER_NAME'] == 'firefox':
			try:
				bconf['profile'] = self.config['PROFILE']
			except KeyError:
				pass

		self.browser = Browser(**bconf)

	def yield_value(self):
		"""Yield value.

		Method provided for convenience. It should typically be used in combination with 'is_loaded' method.

		Yields: Always integer with a value of 1.

		"""
		yield 1

	def find_elements(self, by, selector, obj=None):
		"""Find elements.

		Wrapper around find_by_* methods of Splinter class.

		Args:
			by (str): Criteria to search by.

			selector (str): Value to search for.

			obj (ElementAPI, optional): Instance to apply search filters to. If not specified browser object
			is used.

		Returns:
			ElementList: List of elements that matched the criteria.

		"""
		el = list()

		if obj is None:
			obj = self.browser

		if by == 'css':
			el = obj.find_by_css(selector)
		elif by == 'id':
			el = obj.find_by_id(selector)
		elif by == 'name':
			el = obj.find_by_name(selector)
		elif by == 'tag':
			el = obj.find_by_tag(selector)
		elif by == 'text':
			el = obj.find_by_text(selector)
		elif by == 'value':
			el = obj.find_by_value(selector)
		elif by == 'xpath':
			el = obj.find_by_xpath(selector)
		elif by == 'href':
			el = obj.find_link_by_href(selector)
		elif by == 'p-href':
			el = obj.find_link_by_partial_href(selector)
		elif by == 'l-p-text':
			el = obj.find_link_by_partial_text(selector)
		elif by == 'l-text':
			el = obj.find_link_by_text(selector)
		elif by == 'o-text':
			el = obj.find_option_by_text(selector)
		elif by == 'o-value':
			el = obj.find_option_by_value(selector)

		return el

	def visit(self, url):
		"""None: Visit."""
		self.browser.visit(url)

	def quit(self):
		"""None: Quit."""
		self.browser.quit()

	def is_loaded(self):
		"""Is loaded.

		Verifies if page has been loaded yet.

		Returns:
			bool: True if page has loaded. False otherwise.

		"""
		i = self.browser.html.find('</html>')
		if i == -1:
			return False

		i = self.browser.html.count('<iframe')
		j = self.browser.html.count('</iframe>')
		if i != j:
			return False

		return True

	def wait(self, sec=None):
		"""Wait.

		Wait given number of seconds before checks if page is loaded.

		Args:
			sec (int, optional): Seconds to wait.

		Returns:
			int: Always 0.

		"""
		if isinstance(sec, int):
			sleep(sec)
			return 0

		while not self.is_loaded():
			self.yield_value()
		return 0


