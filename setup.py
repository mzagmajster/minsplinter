from setuptools import setup


setup(
	name='minsplinter',
	version='0.0.1',
	packages=['minsplinter'],
	url='https://gitlab.com/Zag/minsplinter',
	license='MIT license',
	author='Matic Zagmajster',
	author_email='zagm101@gmail.com',
	description='Wrapper for Splinter library.',
	install_requires=['splinter>=0.7.6'],
	extras_require={
		'dev': ['click>=6.7']
	}
)
