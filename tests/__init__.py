from time import sleep
from datetime import datetime
import unittest

from minsplinter import MinSplinter


class TestMinsplinter(unittest.TestCase):
	config = None

	def setUp(self):
		self.config = dict({
			'DRIVER_NAME': 'firefox',
			'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
							'Chrome/58.0.3029.110 Safari/537.36',
			'PROFILE': None
		})

	def tearDown(self):
		pass

	def test_obj_construction(self):
		o = MinSplinter(self.config)
		o.visit('https://www.youtube.com/')

		while not o.is_loaded():
			o.yield_value()

		o.quit()

	def test_is_loaded(self):
		o = MinSplinter(self.config)
		o.visit('https://www.youtube.com/')

		# Give time to load.
		max_seconds = 8
		start_time = datetime.now()
		force_break = False

		while not o.is_loaded() and not force_break:
			if (datetime.now() - start_time) > max_seconds:
				force_break = True
			o.yield_value()

		o.quit()
		self.assertFalse(force_break)

	def test_chrome_web_driver(self):
		d = {
			'DRIVER_NAME': 'chrome'
		}
		o = MinSplinter(d)
		o.visit('https://www.youtube.com/')
		o.quit()

